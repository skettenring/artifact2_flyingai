// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class sKettenringArtifact2Target : TargetRules
{
	public sKettenringArtifact2Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("sKettenringArtifact2");
	}
}
