// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "sKettenringArtifact2HUD.generated.h"

UCLASS()
class AsKettenringArtifact2HUD : public AHUD
{
	GENERATED_BODY()

public:
	AsKettenringArtifact2HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

